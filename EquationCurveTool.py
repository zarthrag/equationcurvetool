#Author-ZarthCode LLC
#Description-Allows the creation/editing of equation driven sketchc curves. Using Fusion360 native parameters and solver. Allows for parametric/explicit curves, and cartesian, polar, cylindrical, and spherical coordinate systems.

import adsk.core, adsk.fusion, adsk.cam, traceback, sys
from enum import Enum
import math, pickle, copy, codecs

# Place to store referenced event handlers, only needed in Python
_handlers = []
_cHandler = []    # Special place for the command created handler, which persists.
_updateList = None  # List of equation curves to update
_curveDialog = None
_updateDialog = None
_command = None
_updateCmd = None

c_commandName = 'EquationCurveCmd'
c_updateCommandName = 'UpdateEquationCurvesCmd'
c_DesignSketchCreate = 'SketchCreatePanel'
c_DesignToolsUtility = 'UtilityPanel'

# Equation Type Enumeration
class CurveType(Enum):
    Parametric = 1  # Uses two equations to evaluate X and Y or r and theta
    Explicit = 2    # Uses one equation to evaluate Y or r and a rage for X or a. 2D curves only.

# Coordinate system enumeration
class CoordinateSystem(Enum):
    Cartesian = 1       # Specifies X and Y coordinates. For 3D curves, Z is also specified.
    Polar = 2           # 2D curves only.
    Cylindrical = 3     # Specifies radial distance (r), azimuthal angle 3D curves only.
    Spherical = 4       # 3D curves only.
    
 # Default values
defaultCurveName = 'EquationCurve'         # TODO - Add a persistent counter to each new curve
defaultXFunction = '0 mm'
defaultYFunction = '0 mm'
defaultZFunction = '0 mm'
defaultRFunction = '0 mm'
defaultThetaFunction = '0 deg'
defaultPhiFunction = '0 deg'
defaultRThetaFunction = '0 mm'
defaultYXFunction = '0 mm'

defaultThetaParam = 'theta'
defaultXParam = 'x'
defaultTParam = 't'

defaultAngleMin = '0 degree'
defaultAngleMax = '180 degree'
defaultAngleStep = '1 degree'

defaultDistanceMin = '0 mm'
defaultDistanceMax = '100 mm'
defaultDistanceStep = '1 mm'

defaultTStart = '0'
defaultTFinish = '360'
defaultTStep = '10'        # TODO - Default step needs to be intelligent, with override option.

defaultCurveType = CurveType.Parametric
defaultCoordinateSystem = CoordinateSystem.Cartesian
default3D = False

defaultStatusMsg = '<div align="center"><b>Status:</b> OK</div>.'
defaultNewCurveItemSelect = "New Curve"
attributeGroupName = "ZarthCodeLLC"
attributeCurveName = "PyEquationCurveName"
attributeCurveData = "PyEquationCurveData"


# Search for a parameter in userParameters or in ModelParameters
def findParameter(paramName):
    app = adsk.core.Application.get()
    design = app.activeProduct
    param = None
    
    if paramName is not "":
        param = design.userParameters.itemByName(paramName)
    else:
        param = None
    
    return param

class CurveDialog:
    """
    Container for all EquationCurve dialog elements for ease of access
    """
    def __init__(self):
        # Curve dictionary
        self.curves = {}
        
        # Curve build status
        self.dirty = True
        
        self.loadCurves()
        
    def initialize(self, cmdInputs):
        app = adsk.core.Application.get()
        design = adsk.fusion.Design.cast(app.activeProduct)
        unitsMgr = adsk.fusion.FusionUnitsManager.cast(design.fusionUnitsManager)
        #Initialize the curve dialog to defaults
        
        inputs = adsk.core.CommandInputs.cast(cmdInputs)
                
        self.curveSelect = inputs.addDropDownCommandInput('curveSelect', 'Create/Edit Equation Curve', adsk.core.DropDownStyles.TextListDropDownStyle)
        self.curveSelect.listItems.add(defaultNewCurveItemSelect, True)
        
        # Add all curves to the dialog.
        for c in self.curves.keys():
            self.curveSelect.listItems.add(c, False)
        
        self.curveName = inputs.addStringValueInput('curveName', 'Equation Curve Name', EquationCurve.GenerateCurveName())
        
        # TODO - Set minimum width based on length of the longest curve name using setDialogInitialSize/setDialogMinimumSize

        self.curveDimensionSelect = inputs.addButtonRowCommandInput('curveDimensionSelect', 'Curve Dimension', False)            
        self.dim2D = self.curveDimensionSelect.listItems.add('2D', not default3D, 'Resources/2DIcon')
        self.dim3D = self.curveDimensionSelect.listItems.add('3D', default3D, 'Resources/3DIcon')
        
        self.curveTypeSelect = inputs.addDropDownCommandInput('curveTypeSelect', 'Curve Type',  adsk.core.DropDownStyles.TextListDropDownStyle)
        self.Parametric = self.curveTypeSelect.listItems.add('Parametric', True)
        self.Explicit = self.curveTypeSelect.listItems.add('Explicit', False)
        
        self.coordinateSystemSelect = inputs.addDropDownCommandInput('coordinateSystemSelect', 'Coordinate System', adsk.core.DropDownStyles.TextListDropDownStyle)
        self.Cartesian = self.coordinateSystemSelect.listItems.add('Cartesian', True)
        self.Polar = self.coordinateSystemSelect.listItems.add('Polar', False)
        self.Cylindrical = self.coordinateSystemSelect.listItems.add('Cylindrical', False)
        self.Spherical = self.coordinateSystemSelect.listItems.add('Spherical', False)
        
        # Update coodinateSystem dropdown entries
        self.dimensionSelectionChanged()
        
        # X(t) 
        self.xFunction = inputs.addValueInput('xFunction','x(t):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultXFunction))
        self.xFunction.isVisible = True
        
        # Y(t)
        self.yFunction = inputs.addValueInput('yFunction','y(t):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultYFunction))
        self.yFunction.isVisible = True
        
        # Z(t) function
        self.zFunction = inputs.addValueInput('zFunction','z(t):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultZFunction))
        self.zFunction.isVisible = False
        
        # r(t)
        self.rFunction = inputs.addValueInput('rFunction','r(t):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultRFunction))
        self.rFunction.isVisible = False
        
        # theta(t)
        self.thetaFunction = inputs.addValueInput('thetaFunction','ϴ(t):', 'deg', adsk.core.ValueInput.createByString(defaultThetaFunction))
        self.thetaFunction.isVisible = False
        
        # phi(t) function
        self.phiFunction = inputs.addValueInput('phiFunction','ɸ(t):', 'deg', adsk.core.ValueInput.createByString(defaultPhiFunction))
        self.phiFunction.isVisible = False
        
        # r(theta) function
        self.rThetaFunction = inputs.addValueInput('rThetaFunction','r(ϴ):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultRThetaFunction))
        self.rThetaFunction.isVisible = False
        
        # Y(X) function
        self.YXFunction = inputs.addValueInput('yXFunction','y(x):', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultYXFunction))
        self.YXFunction.isVisible = False
        
        # Theta Range
        self.thetaRange = inputs.addGroupCommandInput('thetaRange', 'ϴ Range')
        self.thetaParam = self.thetaRange.children.addStringValueInput('thetaParam', 'ϴ Parameter:', defaultThetaParam)
        self.thetaStart = self.thetaRange.children.addValueInput('thetaStart', 'ϴ Start:', 'deg', adsk.core.ValueInput.createByString(defaultAngleMin))
        self.thetaFinish = self.thetaRange.children.addValueInput('thetaFinish', 'ϴ Finish:', 'deg', adsk.core.ValueInput.createByString(defaultAngleMax))
        self.thetaStep = self.thetaRange.children.addValueInput('thetaStep', 'ϴ Step:', 'deg', adsk.core.ValueInput.createByString(defaultAngleStep))
        self.thetaRange.isVisible = False
        
        # X Range
        self.xRange = inputs.addGroupCommandInput('xRange', 'x Range')
        self.xParam = self.xRange.children.addStringValueInput('xParam', 'x Parameter:', defaultXParam)
        self.xStart = self.xRange.children.addValueInput('xStart', 'X Start:', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultDistanceMin))
        self.xFinish = self.xRange.children.addValueInput('xFinish', 'X Finish:', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultDistanceMax))
        self.xStep = self.xRange.children.addValueInput('xStep', 'X Step:', unitsMgr.defaultLengthUnits, adsk.core.ValueInput.createByString(defaultDistanceStep))
        self.xRange.isVisible = False
        
        # t Range
        self.tRange = inputs.addGroupCommandInput('tRange', 't Range')
        self.tParam = self.tRange.children.addStringValueInput('tParam', 't Parameter:', defaultTParam)
        self.tStart = self.tRange.children.addValueInput('tStart', 't Start:', '', adsk.core.ValueInput.createByString(defaultTStart))
        self.tFinish = self.tRange.children.addValueInput('tFinish', 't Finish:', '', adsk.core.ValueInput.createByString(defaultTFinish))
        self.tStep = self.tRange.children.addValueInput('tStep', 't Step:', '', adsk.core.ValueInput.createByString(defaultTStep))
        self.tRange.isVisible = True
        
        # Advanced options.
        self.advanced = inputs.addGroupCommandInput('advancedOptions', 'Advanced Options')
        self.advanced.isExpanded = False
        self.enablePreview = self.advanced.children.addBoolValueInput('enablePreview', 'Enable Preview', True, "", True)
        # TODO - Checkbox to enable/disable curve previews.
        # TODO - Offer smart range selection dialog options
        # TODO - Offer a convenient range parameter edit box (param name, expression, value, unit)
        # TODO - Create a "threshold" or "precision" option to ensure small numbers don't ruin the build routine
        # Error Status
        self.statusMsg = inputs.addTextBoxCommandInput('statusMsg', 'Status', defaultStatusMsg, 2, True)
        self.statusMsg.isFullWidth = True
        
    def load(self):
        """
        Load the selected curve into the dialog.
        """
        # Get the currently selected curve
        curve = self.curves[self.curveSelect.selectedItem.name]
        
        # backfill all of the inputs with the curve's values
        self.curveName.value = copy.deepcopy(curve.curveName)
        
        self.dim2D.isSelected = curve.is2D()
        self.dim3D.isSelected = curve.is3D()
        
        # Update coodinateSystem dropdown entries
        self.dimensionSelectionChanged(curve.coordinateSystem)
        
        self.Parametric.isSelected = (curve.curveType == CurveType.Parametric)
        self.Explicit.isSelected = (curve.curveType == CurveType.Explicit)
        
        if self.Cartesian:
            self.Cartesian.isSelected = (curve.coordinateSystem == CoordinateSystem.Cartesian)
            
        if self.Polar:
            self.Polar.isSelected = (curve.coordinateSystem == CoordinateSystem.Polar)
            
        if self.Cylindrical:
            self.Cylindrical.isSelected = (curve.coordinateSystem == CoordinateSystem.Cylindrical)
            
        if self.Spherical:
            self.Spherical.isSelected = (curve.coordinateSystem == CoordinateSystem.Spherical)

        self.xFunction.value = copy.deepcopy(curve.xFunction)
        self.yFunction.value = copy.deepcopy(curve.yFunction)
        self.zFunction.value = copy.deepcopy(curve.zFunction)

        self.rFunction.value = copy.deepcopy(curve.rFunction)
        self.thetaFunction.value = copy.deepcopy(curve.thetaFunction)
        self.phiFunction.value = copy.deepcopy(curve.phiFunction)
        self.rThetaFunction.value = copy.deepcopy(curve.rThetaFunction)
        self.YXFunction.value = copy.deepcopy(curve.YXFunction)
        
        self.tParam.value = copy.deepcopy(curve.tParam)
        self.tStart.value = copy.deepcopy(curve.tStart)
        self.tFinish.value = copy.deepcopy(curve.tFinish)
        self.tStep.value = copy.deepcopy(curve.tStep)
    
        self.thetaParam.value = copy.deepcopy(curve.thetaParam)
        self.thetaStart.value = copy.deepcopy(curve.thetaStart)
        self.thetaFinish.value = copy.deepcopy(curve.thetaFinish)
        self.thetaStep.value = copy.deepcopy(curve.thetaStep)

        self.xParam.value = copy.deepcopy(curve.xParam)
        self.xStart.value = copy.deepcopy(curve.xStart)
        self.xFinish.value = copy.deepcopy(curve.xFinish)
        self.xStep.value = copy.deepcopy(curve.xStep)
        self.enablePreview = copy.deepcopy(copy.enablePreview)
        self.resetGUI()
        
    def loadCurves(self):
        """
        Load all curves from the active sketch
        """
        app = adsk.core.Application.get()
        design = adsk.fusion.Design.cast(app.activeProduct)
        sketch = adsk.fusion.Sketch.cast(design.activeEditObject)
        for s in sketch.sketchCurves.sketchFittedSplines:
            spline = adsk.fusion.SketchFittedSpline.cast(s)     # Syntatic sugar
            curveName = spline.attributes.itemByName(attributeGroupName, attributeCurveName)
            if curveName != None:
                # Add an entry for the found curve.
                self.curves[curveName.value] = EquationCurve.deserialize(spline)
            
    def buildCurve(self):
        """
        Re/builds the current curve, if necessary.
        """
        
        self.loadCurves()   # Ensure all curve references are upto date.

        # Create a curve object from current dialog settings
        curve = EquationCurve()
        
        # Determine if the curve already exists
        if self.curveName.value in self.curves:
            # Preserve the spline 
            existingCurve = self.curves.get(self.curveName.value, EquationCurve())
            curve.fittedSpline = existingCurve.fittedSpline
        
        if curve != None:
            if False == curve.build():   # Build points and spline
                return None
        
        # Return the newly-constructed spline
        return curve
        
    
    def setStatusMsg(self, message):
        
        self.statusMsg.formattedText = '<div align="center"><b>Status:</b> {}</div>'.format(message)
        
    # Returns True if the parameters are a valid expression
    def validateAll(self):
        app = adsk.core.Application.get()
        design = adsk.fusion.Design.cast(app.activeProduct)
        unitsMgr = adsk.fusion.FusionUnitsManager.cast(design.fusionUnitsManager)

        # Make sure the range parameters are real/valid
        rangeParam = None
        rangeParamInput = None
        rangeMin = None
        rangeMax = None
        rangeStep = None
        rangeUnit = None
        
        if self.is2D() and self.isExplicit() and self.isCartesian():
            rangeParamInput = self.xParam
            rangeMin = self.xStart
            rangeMax = self.xFinish
            rangeStep = self.xStep
            rangeUnit = "cm"   # xRange parameter must work out to a distance unit
        elif self.is2D() and self.isExplicit() and self.isPolar():
            rangeParamInput = self.thetaParam
            rangeMin = self.thetaStart
            rangeMax = self.thetaFinish
            rangeStep = self.thetaStep
            rangeUnit = "rad"   # theta range parameter must work out to an angle unit.
        else:
            rangeParamInput = self.tParam
            rangeMin = self.tStart
            rangeMax = self.tFinish
            rangeStep = self.tStep
            # tRange can be any unit.
        
        rangeParam = findParameter(rangeParamInput.value)
        
        if rangeParam == None:
            # Mark the parameter input as invalid
            self.setStatusMsg("{} is invalid".format(rangeParamInput.name))
            rangeParamInput.isValueError = True
            return False
        else:
            rangeParamInput.isValueError = False

        if rangeUnit == None:
            rangeUnit = rangeParam.unit
        
        # Make sure the range units are set correctly
        rangeMin.unitType = rangeUnit
        rangeMax.unitType = rangeUnit
        rangeStep.unitType = rangeUnit
            
        # Validate the Function definitions
        if self.is2D() and self.isParametric() and self.isCartesian():
            if not self.xFunction.isValidExpression:
                self.setStatusMsg("Function x(t) does not evaluate to a distance unit.")
                return False
                
            if not self.yFunction.isValidExpression:
                self.setStatusMsg("Function y(t) does not evaluate to a distance unit.")
                return False
            
        # 2D, Parametric, Polar
        if self.is2D() and self.isParametric() and self.isPolar():
            if not self.rFunction.isValidExpression:
                self.setStatusMsg("Function r(t) does not evaluate to a distance unit.")
                return False
            else:
                self.rFunction.isValueError = False
                
            if not self.thetaFunction.isValidExpression:
                self.setStatusMsg("Function ϴ(t) does not evaluate to an angle unit.")
                return False
            
        # 2D, Explicit, Cartesian
        if self.is2D() and self.isExplicit() and self.isCartesian():
           if not self.YXFunction.isValidExpression:
                self.setStatusMsg("Function y(x) does not evaluate to a distance unit.")
                return False
            
        # 2D, Explicit, Polar
        if self.is2D() and self.isExplicit() and self.isPolar():
            if not self.rThetaFunction.isValidExpression:
                self.setStatusMsg("Function r(ϴ) does not evaluate to a distance unit.")
                return False
           
        # 3D, Cartesian
        if self.is3D() and self.isCartesian():
            if not self.xFunction.isValidExpression:
                self.setStatusMsg("Function x(t) does not evaluate to a distance unit.")
                return False
                
            if not self.yFunction.isValidExpression:
                self.setStatusMsg("Function y(t) does not evaluate to a distance unit.")
                return False
                
            if not self.zFunction.isValidExpression:
                self.setStatusMsg("Function z(t) does not evaluate to a distance unit.")
                return False
                
        # 3D, Cylindrical
        if self.is3D() and self.isCylindrical():
            if not self.zFunction.isValidExpression:
                self.setStatusMsg("Function z(t) does not evaluate to a distance unit.")
                return False
                 
            if not self.rFunction.isValidExpression:
                self.setStatusMsg("Function r(t) does not evaluate to a distance unit.")
                return False
                
            if not self.thetaFunction.isValidExpression:
                self.setStatusMsg("Function ϴ(t) does not evaluate to an angle unit.")
                return False
            
        # 3D, Spherical
        if self.is3D() and self.isSpherical():
            if not self.rFunction.isValidExpression:
                self.setStatusMsg("Function r(t) does not evaluate to a distance unit.")
                return False
                
            if not self.thetaFunction.isValidExpression:
                self.setStatusMsg("Function ϴ(t) does not evaluate to an angle unit.")
                return False
                
            if not self.phiFunction.isValidExpression:
                self.setStatusMsg("Function ɸ(t) does not evaluate to an angle unit.")
                return False
        
        # Ensure that the range parameter is of the correct unit
        if not unitsMgr.isValidExpression(rangeParamInput.value, rangeUnit) and not rangeUnit == "":
            rangeParamInput.isValueError = True
            self.setStatusMsg("Range parameter unit is {}".format(rangeUnit))
            print("range parameter unit value error")
        else:
            rangeParamInput.isValueError = False
        
        # range Min, Max, and step must be valid numbers and have matching units
        if not rangeMin.isValidExpression:
            self.setStatusMsg("Minimum range should be {}".format(rangeUnit))
            return False
            
        if not rangeMax.isValidExpression:
            self.setStatusMsg("Maximum range should be {}".format(rangeUnit))
            return False
            
        if not rangeStep.isValidExpression:
            self.setStatusMsg("Range step should be {}".format(rangeUnit))
            return False
    
        # (Finish - Start)/Step must equal a positive number
        rangeExpression = "({}-{})/{}".format(rangeMax.value,rangeMin.value,rangeStep.value)
        if not unitsMgr.isValidExpression(rangeExpression,rangeUnit):
            self.setStatusMsg("Invalid Range: (Max-Min)/Steps must evaluate to a positive number.")
            return False
        elif unitsMgr.evaluateExpression(rangeExpression, rangeUnit) <= 0:
            self.setStatusMsg("Invalid Range: (Max-Min)/Steps must evaluate to a positive number.")
            return False
        
        # Ensure that the name of the curve is unique.
        if self.curveName.value in self.curves.keys() and self.curveSelect.selectedItem.name != defaultNewCurveItemSelect and self.curveSelect.selectedItem.name != self.curveName.value:
            self.curveName.isValueError = True
            self.setStatusMsg("Equation curve name must be unique")
        else:
            self.curveName.isValueError = False
        return True
              
        
    def is2D(self):
        return self.curveDimensionSelect.selectedItem.name == '2D'
        
    def is3D(self):
        return self.curveDimensionSelect.selectedItem.name == '3D'
        
    def isParametric(self):
        return self.curveTypeSelect.selectedItem.name == 'Parametric'
        
    def isExplicit(self):
        return self.curveTypeSelect.selectedItem.name == 'Explicit'
        
    def isPolar(self):
        return self.coordinateSystemSelect.selectedItem.name == 'Polar'
        
    def isCartesian(self):
        return self.coordinateSystemSelect.selectedItem.name == 'Cartesian'
        
    def isCylindrical(self):
        return self.coordinateSystemSelect.selectedItem.name == 'Cylindrical'
        
    def isSpherical(self):
        return self.coordinateSystemSelect.selectedItem.name == 'Spherical'
        
    def curveType(self):
        if self.isParametric():
            return CurveType.Parametric
        elif self.isExplicit():
            return CurveType.Explicit
            
    def coordinateSystem(self):
        if self.isCartesian():
            return CoordinateSystem.Cartesian
        elif self.isPolar():
            return CoordinateSystem.Polar
        elif self.isCylindrical():
            return CoordinateSystem.Cylindrical
        elif self.isSpherical():
            return CoordinateSystem.Spherical
            
    def dimensionSelectionChanged(self, default=CoordinateSystem.Cartesian):
        if self.curveDimensionSelect.selectedItem.name == self.dim2D.name:
            # Switch available curveTypes
            self.curveTypeSelect.isVisible = True
            
            # Switch available coordinate systems
            self.coordinateSystemSelect.listItems.clear()
            self.Cartesian = self.coordinateSystemSelect.listItems.add('Cartesian', (default == CoordinateSystem.Cartesian))     # TODO - If prev selection was cartesian, maintain, polar, then go to cylindrical/spherical
            self.Polar = self.coordinateSystemSelect.listItems.add('Polar', (default == CoordinateSystem.Polar))
            self.Cylindrical = None
            self.Spherical = None
        
        elif self.curveDimensionSelect.selectedItem.name == self.dim3D.name:
            # Hide available curveTypes
            self.curveTypeSelect.isVisible = False
            
            # Switch available coordinate systems
            self.coordinateSystemSelect.listItems.clear()
            self.Cartesian = self.coordinateSystemSelect.listItems.add('Cartesian', (default == CoordinateSystem.Cartesian))
            self.Cylindrical = self.coordinateSystemSelect.listItems.add('Cylindrical', (default == CoordinateSystem.Cylindrical))
            self.Spherical = self.coordinateSystemSelect.listItems.add('Spherical', (default == CoordinateSystem.Spherical))
            self.Polar = None
            
    # Reset function inputs according to settings    
    def resetGUI(self):
        
        # Save some duplicate lines.        
        self.xFunction.isVisible = False
        self.yFunction.isVisible = False
        self.zFunction.isVisible = False
        self.rFunction.isVisible = False
        self.thetaFunction.isVisible = False
        self.phiFunction.isVisible = False
        self.rThetaFunction.isVisible = False
        self.YXFunction.isVisible = False
        
        self.thetaRange.isVisible = False
        self.xRange.isVisible = False
        self.tRange.isVisible = False
        
        # 2D, Parametric, Cartesian
        if self.is2D() and self.isParametric() and self.isCartesian():
            self.xFunction.isVisible = True
            self.yFunction.isVisible = True
            self.tRange.isVisible = True
            
        # 2D, Parametric, Polar
        if self.is2D() and self.isParametric() and self.isPolar():
            self.rFunction.isVisible = True
            self.thetaFunction.isVisible = True
            self.tRange.isVisible = True
            
        # 2D, Explicit, Cartesian
        if self.is2D() and self.isExplicit() and self.isCartesian():
            self.YXFunction.isVisible = True
            self.xRange.isVisible = True
            
        # 2D, Explicit, Polar
        if self.is2D() and self.isExplicit() and self.isPolar():
            self.rThetaFunction.isVisible = True
            self.thetaRange.isVisible = True
            
        # 3D, Cartesian
        if self.is3D() and self.isCartesian():
            self.xFunction.isVisible = True
            self.yFunction.isVisible = True
            self.zFunction.isVisible = True
            self.tRange.isVisible = True
            
        # 3D, Cylindrical
        if self.is3D() and self.isCylindrical():
            self.zFunction.isVisible = True
            self.rFunction.isVisible = True
            self.thetaFunction.isVisible = True
            self.tRange.isVisible = True
            
        # 3D, Spherical
        if self.is3D() and self.isSpherical():
            self.rFunction.isVisible = True
            self.thetaFunction.isVisible = True
            self.phiFunction.isVisible = True
            self.tRange.isVisible = True

# Range fucntion that accepts floating point numbers.
def frange(start, stop, step):
    """
    Range function that accepts floats
    Usage:
    frange(-2, 2, 0.1)
    frange(10)
    frange(10, step = 0.5)
    
    The returned value is an iterator. Use list(frange) for a list.
    """
    
    if stop is None:
        stop, start = start, 0.
    else:
        start = float(start)
    
    count = int(math.ceil((stop-start)/step)) + 1
    return (start + n*step for n in range(count)) 

class StrToBytes:
    def __init__(self, fileobj):
        self.fileobj = fileobj
    def read(self, size):
        return self.fileobj.read(size).encode()
    def readline(self, size=-1):
        return self.fileobj.readline(size).encode()
    
def PerpendicularDisance(P, A, B):

    # Three dimensional
    # Determine 3d vector
    dx = B.x - A.x
    dy = B.y - A.y
    dz = B.z - A.z
    
    # The case of comparing the distance of two points
    if A.isEqualTo(B):
        # The distance between two points
        return (((P.x - A.x)**2)+((P.y - A.y)**2)+((P.z - A.z)**2))**0.5
    
    # calculate the t that minimizes the distance
    t = (((P.x - A.x)*dx) + ((P.y - A.y)*dy) + ((P.z - A.z)*dz))/(dx**2 + dy**2 + dz**2)
    
    # Find the vector between P and the point described by t
    rx = P.x - (A.x + t * dx)
    ry = P.y - (A.y + t * dy)
    rz = P.z - (A.z + t * dz)
    
    return ( rx**2 + ry**2 + rz**2 )**0.5

# Recursive algorithm that decimates a curve composed of line segments to a similar curve with fewer points.
def DouglasPeucker(PointList, epsilon):
    
    # Find the point with the maximum distance
    dmax = 0
    dmaxIdx = 0
    
    for idx, point in enumerate(PointList, 1):
        dist = PerpendicularDisance(adsk.core.Point3D.create(point[0],point[1],point[2]), adsk.core.Point3D.create(PointList[0][0],PointList[0][1],PointList[0][2]), adsk.core.Point3D.create(PointList[-1][0],PointList[-1][1],PointList[-1][2]))
        if dist > dmax:
            dmax = dist
            dmaxIdx = idx
    
    # If the max distance is greater than epsilon, recursively simplify
    if (dmax > epsilon):
        # Recursively  simplify on both sides of the index
        recResults1 = DouglasPeucker(PointList[:dmaxIdx+1],epsilon)
        recResults2 = DouglasPeucker(PointList[dmaxIdx:], epsilon)
        
        # Bulid the new result list
        return recResults1[:-1] + recResults2
        
    # Simplify this set and return it
    return [PointList[0],PointList[-1]]
    
class EquationCurve():
    def __init__(self):
        self.curveName = copy.deepcopy(_curveDialog.curveName.value)
        self.curveType = copy.deepcopy(_curveDialog.curveType())
        self.coordinateSystem = _curveDialog.coordinateSystem()
        self.dimension3D = _curveDialog.is3D()

        self.xFunction = copy.deepcopy(_curveDialog.xFunction.expression)
        self.yFunction = copy.deepcopy(_curveDialog.yFunction.expression)
        self.zFunction = copy.deepcopy(_curveDialog.zFunction.expression)

        self.rFunction = copy.deepcopy(_curveDialog.rFunction.expression)
        self.thetaFunction = copy.deepcopy(_curveDialog.thetaFunction.expression)
        self.phiFunction = copy.deepcopy(_curveDialog.phiFunction.expression)
        self.rThetaFunction = copy.deepcopy(_curveDialog.rThetaFunction.expression)
        self.YXFunction = copy.deepcopy(_curveDialog.YXFunction.expression)
        
        self.tParam = copy.deepcopy(_curveDialog.tParam.value)
        self.tStart = copy.deepcopy(_curveDialog.tStart.expression)
        self.tFinish = copy.deepcopy(_curveDialog.tFinish.expression)
        self.tStep = copy.deepcopy(_curveDialog.tStep.expression)
    
        self.thetaParam = copy.deepcopy(_curveDialog.thetaParam.value)
        self.thetaStart = copy.deepcopy(_curveDialog.thetaStart.expression)
        self.thetaFinish = copy.deepcopy(_curveDialog.thetaFinish.expression)
        self.thetaStep = copy.deepcopy(_curveDialog.thetaStep.expression)

        self.xParam = copy.deepcopy(_curveDialog.xParam.value)
        self.xStart = copy.deepcopy(_curveDialog.xStart.expression)
        self.xFinish = copy.deepcopy(_curveDialog.xFinish.expression)
        self.xStep = copy.deepcopy(_curveDialog.xStep.expression)
        
        self.enablePreview = copy.deepcopy(_curveDialog.enablePreview.value)
        
        self.points = None
        self.fittedSpline = None
        self.constraints = None
        
    def __getstate__(self):
        """
        Method to control pickling/serializing the object for storage as an attribute.
        """
        # Omit the references, they cannot be stored
        tempPoints, self.points = self.points, None
        tempFittedSpline, self.fittedSpline = self.fittedSpline, None
        
        state = self.__dict__.copy()
        
        # Restore the references
        self.points = tempPoints
        self.fittedSpline = tempFittedSpline
        
        return state
    def __setstate__(self, state):
        """
        Perform initialization after being unpickled/deserialized.
        """
        self.curveName = None
        self.curveType = None
        self.coordinateSystem = None
        self.dimension3D = None

        self.xFunction = None
        self.yFunction = None
        self.zFunction = None

        self.rFunction = None
        self.thetaFunction = None
        self.phiFunction = None
        self.rThetaFunction = None
        self.YXFunction = None
        
        self.tParam = None
        self.tStart = None
        self.tFinish = None
        self.tStep = None
    
        self.thetaParam = None
        self.thetaStart = None
        self.thetaFinish = None
        self.thetaStep = None

        self.xParam = None
        self.xStart = None
        self.xFinish = None
        self.xStep = None
        
        self.points = None
        self.fittedSpline = None
        
        self.enablePreview = None
        #app = adsk.core.Application.get()
        #ui = app.userInterface
        #design = adsk.fusion.Design.cast(app.activeProduct)
        
        # Restore state
        self.__dict__.update(state)
        self.constraints = None
        
        # TODO - Find the corresponding fittedSpline, and store the reference.
        
        
        
    def is2D(self):
        return self.dimension3D == False
        
    def is3D(self):
        return self.dimension3D == True
        
    def isParametric(self):
        return self.curveType == CurveType.Parametric
        
    def isExplicit(self):
        return self.curveType == CurveType.Explicit
        
    def isPolar(self):
        return self.coordinateSystem == CoordinateSystem.Polar
        
    def isCartesian(self):
        return self.coordinateSystem == CoordinateSystem.Cartesian
        
    def isCylindrical(self):
        return self.coordinateSystem == CoordinateSystem.Cylindrical
        
    def isSpherical(self):
        return self.coordinateSystem == CoordinateSystem.Spherical
        
    # Build a point list from the curve information
    def build(self):
        
        app = adsk.core.Application.get()
        ui = app.userInterface
        design = adsk.fusion.Design.cast(app.activeProduct)
        unitsMgr = adsk.fusion.FusionUnitsManager.cast(design.fusionUnitsManager)

        # Use the units manager to evaluate each step, and store the point result
        #print('Curve.build() called.')
        self.points = None
        
        # Build a points list using the proper strategy
        # 2D, Parametric, Cartesian
        if self.is2D() and self.isParametric() and self.isCartesian():
            
            # Evaludate xFunction & yFunction along tRange
            rangeParam = findParameter(self.tParam)
            start = unitsMgr.evaluateExpression(self.tStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.tFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.tStep, rangeParam.unit)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [self.xFunction, self.yFunction])
                
        # 2D, Parametric, Polar
        elif self.is2D() and self.isParametric() and self.isPolar():

            # Evaluate rFunction & thetaFunction along tRange
            rangeParam = findParameter(self.tParam)
            start = unitsMgr.evaluateExpression(self.tStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.tFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.tStep, rangeParam.unit)
            
            # Prep the polar equation for conversion to cartesian coordinates
            # x = r*cos(theta)
            # y = r*sin(theta)
            xPolarToCartesian = "({})*cos({})".format(self.rFunction,self.thetaFunction)
            yPolarToCartesian = "({})*sin({})".format(self.rFunction,self.thetaFunction)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [xPolarToCartesian, yPolarToCartesian])
            
        # 2D, Explicit, Cartesian
        elif self.is2D() and self.isExplicit() and self.isCartesian():
            # Evaluate YXFunction along xRange
            rangeParam = findParameter(self.xParam)
            start = unitsMgr.evaluateExpression(self.xStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.xFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.xStep, rangeParam.unit)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [self.YXFunction])
            
        # 2D, Explicit, Polar
        elif self.is2D() and self.isExplicit() and self.isPolar():
            # Evaluate rThetaFunction along thetaRange
            rangeParam = findParameter(self.thetaParam)
            start = unitsMgr.evaluateExpression(self.thetaStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.thetaFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.thetaStep, rangeParam.unit)
            
            # Prep the polar equation for conversion to cartesian coordinates
            # x = r*cos(theta)
            # y = r*sin(theta)
            xPolarToCartesian = "({})*cos({})".format(self.rThetaFunction,self.thetaParam)  # TODO - Optimize this to eliminate evaluating rThetaFunction and thetaParam twice.
            yPolarToCartesian = "({})*sin({})".format(self.rThetaFunction,self.thetaParam)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [xPolarToCartesian, yPolarToCartesian])
            
            
        # 3D, Cartesian
        elif self.is3D() and self.isCartesian():
            # Evaluate xFunction, yFunction, and zFunction along tRange.
            rangeParam = findParameter(self.tParam)
            start = unitsMgr.evaluateExpression(self.tStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.tFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.tStep, rangeParam.unit)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [self.xFunction, self.yFunction, self.zFunction])
            
        # 3D, Cylindrical
        elif self.is3D() and self.isCylindrical():
            # Evaluate rFunction, thetaFunction, and zFunction along tRange
            rangeParam = findParameter(self.tParam)
            start = unitsMgr.evaluateExpression(self.tStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.tFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.tStep, rangeParam.unit)
            
            # Prep the polar equation for conversion to cartesian coordinates
            # x = r*cos(theta)
            # y = r*sin(theta)
            xPolarToCartesian = "({})*cos({})".format(self.rFunction,self.thetaFunction)
            yPolarToCartesian = "({})*sin({})".format(self.rFunction,self.thetaFunction)
            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [xPolarToCartesian, yPolarToCartesian, self.zFunction])
            
        # 3D, Spherical
        elif self.is3D() and self.isSpherical():
            # Evaluate rFunction, thetaFunction, and phiFunction along tRange
            rangeParam = findParameter(self.tParam)
            start = unitsMgr.evaluateExpression(self.tStart, rangeParam.unit)
            finish = unitsMgr.evaluateExpression(self.tFinish, rangeParam.unit)
            step = unitsMgr.evaluateExpression(self.tStep, rangeParam.unit)
            
            # Prep the spherical equation for conversion to cartesian coordinates
            # x = r*sin(theta)*cos(phi)
            # y = r*sin(theta)*sin(phi)
            # z = r*cos(phi)
            xSphericalToCartesian = "({})*sin({})*cos({})".format(self.rFunction,self.thetaFunction, self.phiFunction)
            ySphericalToCartesian = "({})*sin({})*sin({})".format(self.rFunction,self.thetaFunction, self.phiFunction)
            zSphericalToCartesian = "({})*cos({})".format(self.rFunction, self.phiFunction)
            # TODO - Optimize evaluateRange() to avoid evaluating rFunction three times, thetaFunction twice, and phiFunction twice....per point.            
            self.points = EquationCurve.evaluateRange(rangeParam, start, finish, step, [xSphericalToCartesian, ySphericalToCartesian, zSphericalToCartesian])
            
        else:
            ui.messageBox("Curve.build() - dimension3D = {}\ncoordinateSystem = {}\ncurveType = {}".format(self.dimension3D,self.coordinateSystem,self.curveType), "EquationCurve Internal Error")
                
        if self.points != None:
            #print("curve.built() produced {} points".format(self.points.count))
            
            # (re)create a fitted spline.
            try:
                sketch = None
                
                if self.fittedSpline != None:
                    sketch = self.fittedSpline.parentSketch
                    self.fittedSpline.deleteMe()
                else:
                    sketch = adsk.fusion.Sketch.cast(app.activeEditObject)
                 
                sketch.isComputeDeferred = True
                self.fittedSpline = sketch.sketchCurves.sketchFittedSplines.add(self.points)     # TODO - Evaluate the first/last point for merge/close
                self.fittedSpline.isFixed = True
                sketch.isComputeDeferred = False
                if self.fittedSpline == None:
                    ui.messageBox("curve.build() - Spline creation failed!")
                    return False
  
            except:
                return False
                
            return True
                
                
                # Rebuild constraints, if necessary
                #self.popConstraints()
                # coincident
                # tangent
                # fix/unfix
                # curvature
        else:
            ui.messageBox("curve.build() failed to produce points")
            
    def pushConstraints(self):
        # TODO - Implement saving constraints
        self.constraints = []
        
        # Iterate through the constraints
        for c in self.fittedSpline.geometricConstraints:
            # Coincident Constraint
            if c.objectType == adsk.fusion.CoincidentConstraint.classType():
                coincident = adsk.fusion.CoincidentConstraint.cast(c)
                point = coincident.point
                entity = coincident.entity
                
                for e in point.connectedEntities:
                    print("\tconnected to {}".format(e.classType()))
                
                self.constraints.append([c.objectType, point])
                # Delete the old constraint
                #if coincident.isDeletable:
                    #coincident.deleteMe()
                    
                #else:
                    #print("constraint delete failed")
                
            elif c.objectType == adsk.fusion.TangentConstraint.classType():
                tangent = adsk.fusion.TangentConstraint.cast(c)
                curveOne = None
                
            # Fix/Unfix constraint
                
            # Curvature constraint
                
    def popConstraints(self):
        # TODO - Implement restoring constraints
        if self.constraints != None:
            app = adsk.core.Application.get()
            activeSketch = adsk.fusion.Sketch.cast(app.activeEditObject)
            for objectType, entity in self.constraints:
                if objectType == adsk.fusion.CoincidentConstraint.classType():
                    activeSketch.geometricConstraints.addCoincident(entity, self.fittedSpline)
                elif objectType == adsk.fusion.TangentConstraint.classType():
                    activeSketch.geometricConstraints.addTangent(entity, self.fittedSpline)
            self.constraints = None
        
    def toggleShowFitPoints(self, state = None):
        if self.fittedSpline:
            self.fittedSpline.isVisible = not self.fittedSpline.isVisible if state == None else state
    
    @classmethod
    def ToPointCollection(cls, PointList):
        """
        Converts a python list of vectors into an ObjectCollection for creating a spline
        PointList
        
        returns an ObjectCollection of Point3D objects.
        """
        result = adsk.core.ObjectCollection.create()
        for point in PointList:
            result.add(adsk.core.Point3D.create(point[0],point[1],point[2]))
        
        return result
        
    @classmethod
    def evaluateRange(cls, param, start, finish, step, funcs):
        """
        Evaluates a list of functions across the provided range.
        param: Fusion range parameter (used by the functions to step)
        start: Starting value for the range sweep
        finish: Ending value for the range sweep
        step: Amount to increase the value of 'param' each step.
        funcs: A list of functions to evaluate for each element in a point 
        For 1 functions: Point2D(x, funcs[0]->y)
        For 2 functions: Point2D(funcs[0]->x, funcs[1]->y)
        For 3 functions: Point3D(funcs[0]->x, funcs[1]->y, funcs[2]->z)
        
        Returns an ObjectCollection of Point2D or Point3D objects.
        """
        app = adsk.core.Application.get()
        design = adsk.fusion.Design.cast(app.activeProduct)
        unitsMgr = adsk.fusion.FusionUnitsManager.cast(design.fusionUnitsManager)

        # Store param values
        tempValue = param.value # temporiarly store the starting values for rangeParam
        tempExpression = param.expression
    
        # Create a list of points
        points = []
        
        #for idx, f in enumerate(funcs):
        #    print("Evaluating Function [{}]: {}".format(idx, f))
        
        # Evaluate the provided list of functions across the provided range
        for t in frange(start, finish, step):
            param.value = t
            if len(funcs) == 1:
                # [t, func0]
                points.append([unitsMgr.convert(param.value, param.unit, unitsMgr.internalUnits), unitsMgr.evaluateExpression(funcs[0]), 0])
                
            elif len(funcs) == 2:
                # [func0, func1]
                points.append([unitsMgr.evaluateExpression(funcs[0]), unitsMgr.evaluateExpression(funcs[1]), 0])
                
            elif len(funcs) == 3:
                # [func0, func1, func2]
                points.append([unitsMgr.evaluateExpression(funcs[0]), unitsMgr.evaluateExpression(funcs[1]), unitsMgr.evaluateExpression(funcs[2])])
    
        # restore temp variables
        param.value = tempValue
        param.expression = tempExpression

        # TODO - Optional processing of the point list before coversion to a spline. 
        if(False):
            # Decimate the curve data, this really needs a delta.
            points = DouglasPeucker(points, 0.74)
        #print("Results:")
        #for idx, point in enumerate(points):
        #    print("{}: {}".format(idx,point))
            
        # TODO - Consider moving this call into build()
        return EquationCurve.ToPointCollection(points)
        
    # Load curve from attribute
    @classmethod
    def deserialize(cls, spline):
        # TODO - Deserialization routine. 
        curveDataAttr = spline.attributes.itemByName(attributeGroupName, attributeCurveData)
        curve = None
        curve = pickle.loads(codecs.decode(curveDataAttr.value.encode(),"base64"))
        curve.fittedSpline = spline
        #print("Loaded RAW Pickled Attribute:\n{}".format(curveDataAttr.value))
        #print("Encoded RAW Pickled Attribute:\n{}".format(curveDataAttr.value.encode()))
        #print("Decoded RAW Pickled Attribute:\n{}".format(codecs.decode(curveDataAttr.value.encode(),"base64")))
        #print("Deserialized curve:\n")
        #for i in curve.__dict__:
        #    print(i, curve.__dict__[i])
        return curve
        
    # Generates a new, unique curve name
    @staticmethod
    def GenerateCurveName():
        newName = defaultCurveName
        
        # TODO - Determine global number of equation curves and add one.
        newName += str(len(_curveDialog.curves.keys()))
        return newName
    
    # Save curve to an attribute
    def serialize(self):
        #print("Serializing curve:\n")
        #for i in self.__dict__:
        #    print(i, self.__dict__[i])
        
        if(self.fittedSpline != None):
            
            # Store serialized curve into the corresponding fittedSpline
            pickled = pickle.dumps(self,0)
            encPickled = codecs.encode(pickled,"base64")
            decPickled = encPickled.decode()
            #pickled = codecs.encode(pickle.dumps(self, 0),"base64").decode()
            #print("Pickled Curve:\n{}".format(pickled))
            #print("Encoded Pickled Curve:\n{}".format(encPickled))
            #print("Decoded Pickled Curve:\n{}".format(decPickled))
            self.fittedSpline.attributes.add(attributeGroupName, attributeCurveName, self.curveName)
            self.fittedSpline.attributes.add(attributeGroupName, attributeCurveData, str(decPickled))
            # TODO - Add version information
         
# Command Execution Handler
class EquationCurveToolExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            #print("EquationCurveToolExecuteHandler")
            app = adsk.core.Application.get()            
            ui  = app.userInterface
            
            # Get the command that was executed.
            eventArgs = adsk.core.CommandEventArgs.cast(args)
            # cmd = eventArgs.command
            
            if _curveDialog.dirty is True:
            
                activeSketch = adsk.fusion.Sketch.cast(app.activeEditObject)
               
                # Build the curve and serialize it.
                curve =  _curveDialog.buildCurve()
                curve.serialize()
                curve.fittedSpline.isFixed = True
                
                _curveDialog.dirty = False
            
            #if ui:
                #ui.messageBox('EquationCurveToolExecuteHandler called')
        except:
            if ui:
                ui.messageBox('EquationCurveToolExecuteHandler.notify failed:\n{}'.format(traceback.format_exc()))
            activeSketch.isComputeDeferred = False
            eventArgs.executeFailed = True
            eventArgs.executeFailedMessage = 'EquationCurveToolExecuteHandler.notify failed:\n{}'.format(traceback.format_exc())

# Command Execution Preview Handler
class EquationCurveToolExecutePreviewHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
            
    def notify(self, args):
        app = adsk.core.Application.get()
        ui  = app.userInterface  
        eventArgs = adsk.core.CommandEventArgs.cast(args)          
        
        if (_curveDialog.dirty is False and eventArgs.isValidResult is True) or not _curveDialog.enablePreview.value:
            eventArgs.isValidResult = False
            return
            
        try:
            # TODO - Only rebuild the curve if it's marked "dirty".
            activeSketch = adsk.fusion.Sketch.cast(app.activeEditObject)
            #print('EquationCurveToolExecutePreviewHandler')
            # unitsMgr = app.activeProduct.unitsManager
            
            # Get the command that was executed.
            
            # cmd = adsk.core.Command.cast(eventArgs.command)
            # inputs = adsk.core.CommandInputs.cast(cmd.commandInputs)
            
            # TODO - (Re)build the curve, as a construction/highlight.
            #if _curveDialog.dirty == True:
            curve = _curveDialog.buildCurve()
            
            if curve is not None:
                curve.serialize()
            
                # TODO - Make the preview spline standout more.
                curve.fittedSpline.isFixed = True
                
                #_curveDialog.dirty = False
                #eventArgs.isValidResult = False
                #curve.toggleShowFitPoints()
                _curveDialog.dirty = False
                eventArgs.isValidResult = True
                
            else:
                eventArgs.isValidResult = False
                
            
            
        except:
            eventArgs.isValidResult = False
                
            if ui:
                ui.messageBox('EquationCurveToolExecutePreviewHandler.notify failed:\n{}'.format(traceback.format_exc()))
            activeSketch.isComputeDeferred = False

# TODO - Command Started Handler
class EquationCurveActivateHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self,args):
        app = adsk.core.Application.get()
        ui  = app.userInterface
        
        # Get the command that was executed.
        # eventArgs = adsk.core.CommandEventArgs.cast(args)
        # cmd = eventArgs.command
        ui.messageBox("Activate")

class EquationCurveDeactivateHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self,args):
        app = adsk.core.Application.get()
        ui  = app.userInterface
        
        # Get the command that was executed.
        eventArgs = adsk.core.CommandEventArgs.cast(args)
        # cmd = eventArgs.command
        ui.messageBox("Deactivate")
               
# Command Destroy Event Handler                
class EquationCurveToolDestroyHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        global _curveDialog
        #app = adsk.core.Application.get()
        #ui  = app.userInterface
        try:
            # Get the command that was executed.
            eventArgs = adsk.core.CommandEventArgs.cast(args)
            # cmd = eventArgs.command
            
            # Process Termination Reason
            if eventArgs.terminationReason == adsk.core.CommandTerminationReason.AbortedTerminationReason: 	
            # 3 	The command is terminated by clicking OK button, and executed failed.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.CancelledTerminationReason:
            #2 	The command is terminated by clicking Cancel button.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.CompletedTerminationReason:
            #1 	The command is terminated by clicking OK button, and executed successfully.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.PreEmptedTerminationReason:
            #4 	The command is terminated by activating another command.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.SessionEndingTerminationReason:
            #5 	The command is terminated by closing the document.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.UnknownTerminationReason:
            #0 	The command is terminated out of the reasons list below.
                pass
            
            # Handle destruction of all elements
            _handlers.clear()
            _curveDialog = None
            
        except:
            _handlers.clear()
            _curveDialog = None
            #if ui:
                #ui.messageBox('EquationCurveToolDestroyHandler.notify failed:\n{}'.format(traceback.format_exc()))
            #eventArgs.terminationReason
            #eventArgs.executeFailedMessage = 'EquationCurveToolExecuteHandler.notify failed:\n{}'.format(traceback.format_exc())

# Event handler that reacts to any changes the user makes to any of the command inputs
class EquationCurveToolValidateInputsHandler(adsk.core.ValidateInputsEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        app = adsk.core.Application.get()
        ui = app.userInterface
        try:
            #print("EquationCurveToolValidateInputsHandler")
            
            # design = app.activeProduct
            # unitsMgr = design.unitsManager
            
            # Get the inputs to be validated.
            eventArgs = adsk.core.ValidateInputsEventArgs.cast(args)
            # inputs = eventArgs.inputs
                  
            # TODO - If the curveName matches a different existing curveName - mark as invalid/duplicate
            
            # Validate the curve dimension/type/coordinate selections.
            # Validate the range
            # Validate the functions that are about to use
            # 2D, Parametric, Cartesian
            if _curveDialog.dirty:
                if not _curveDialog.validateAll():
                    eventArgs.areInputsValid = False
                else:
                    eventArgs.areInputsValid = True
                    _curveDialog.setStatusMsg("OK")
                            
        except:
            if ui:
                ui.messageBox('EquationCurveToolValidateInputsHandler.notify failed:\n{}'.format(traceback.format_exc()))
                
class EquationCurveToolInputChangedHandler(adsk.core.InputChangedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        #print("EquationCurveToolInputChangedHandler")
        eventArgs = adsk.core.InputChangedEventArgs.cast(args)        
        changedInput = adsk.core.CommandInput.cast(eventArgs.input)
        #print("changedInput.id == {}".format(changedInput.id))
        # curveDimensionSelect change
        if changedInput.id == _curveDialog.curveDimensionSelect.id:            
            _curveDialog.dimensionSelectionChanged()            
            _curveDialog.resetGUI()
            _curveDialog.dirty = True
                
        elif (changedInput.id == _curveDialog.curveTypeSelect.id) or (changedInput.id == _curveDialog.coordinateSystemSelect.id):
            _curveDialog.resetGUI()
            _curveDialog.dirty = True
        elif changedInput.id in [_curveDialog.statusMsg.id,_curveDialog.advanced.id, _curveDialog.enablePreview.id]:
            # Move along, nothing to see here.
            return 
        elif changedInput.id == _curveDialog.curveSelect.id:
            # User selected a different curve, load it.
            #print("changedInput.id == {}".format(changedInput.id))
            _curveDialog.load()
            _curveDialog.dirty = True
        elif changedInput.id in [ _curveDialog.xFunction.id, _curveDialog.yFunction.id, _curveDialog.zFunction.id, _curveDialog.phiFunction.id,
                                 _curveDialog.rFunction.id, _curveDialog.rThetaFunction.id, _curveDialog.thetaFunction.id, _curveDialog.YXFunction.id,
                                 _curveDialog.tParam.id, _curveDialog.tStart.id, _curveDialog.tFinish.id, _curveDialog.tStep.id,
                                 _curveDialog.thetaParam.id, _curveDialog.thetaStart.id, _curveDialog.thetaFinish.id, _curveDialog.thetaStep.id,
                                 _curveDialog.xParam.id, _curveDialog.xStart.id, _curveDialog.xFinish.id, _curveDialog.xStep.id]:
                                     
            _curveDialog.dirty = True
        else:
            
            print("EquationCurveToolInputChangedHandler unimplemented for altered control: "+changedInput.id)
            # TODO - Make this into a proper exception (along with other print notices)
            
# Event handler that reacts wheh the command defintion is executed, which results
# in the command being created and this event being fired.
class EquationCurveToolCommandCreatedHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            app = adsk.core.Application.get()
            ui = app.userInterface
            
            #print("EquationCurveToolCommandCreatedHandler")
            #ui.messageBox("EquationCurveToolCommandCreatedHandler")
            global _curveDialog
            
            if app.activeEditObject.objectType != adsk.fusion.Sketch.classType():
                
                ui.messageBox('A sketch must be active.', 'Cannot Create Equation Curve')
                return False
            
            # Reserved Parameter setup
            # thetaStart thetaFinish thetaStep
            
            # xStart xFinish xStep
            # tStart tFinish tStep
            
            # Event handler setup
                
            eventArgs = adsk.core.CommandCreatedEventArgs.cast(args)
            cmd = adsk.core.Command.cast(eventArgs.command)
            cmd.isRepeatable = True
            cmd.setDialogInitialSize(425,300)
            
            # Setup command event handlers
            onExecute = EquationCurveToolExecuteHandler()
            cmd.execute.add(onExecute)
            
            onExecutePreview = EquationCurveToolExecutePreviewHandler()
            cmd.executePreview.add(onExecutePreview)
            
            onDestroy = EquationCurveToolDestroyHandler()
            cmd.destroy.add(onDestroy)
            
            onValidate = EquationCurveToolValidateInputsHandler()
            cmd.validateInputs.add(onValidate)
            
            onChange = EquationCurveToolInputChangedHandler()
            cmd.inputChanged.add(onChange)
            
            onActivate = EquationCurveActivateHandler()
            cmd.activate.add(onActivate)
            
            onDeactivate = EquationCurveDeactivateHandler()
            cmd.deactivate.add(onDeactivate)
                        
            # Store python handler references.
            _handlers.append(onExecute)
            _handlers.append(onExecutePreview)
            _handlers.append(onDestroy)
            _handlers.append(onValidate)
            _handlers.append(onChange)
            
            # Create input GUI
            inputs = cmd.commandInputs
            
            _curveDialog = CurveDialog()
            _curveDialog.initialize(inputs)
            
            # Obtain the equation curves in this sketch instance
            
            # If there is exactly one existing curve, preselect it.
            
        except:
            if ui:
                ui.messageBox('EquationCurveToolCommandCreatedHandler.notify failed:\n{}'.format(traceback.format_exc()))

class UpdateEquationCurvesDestroyHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        global _updateDialog
        global _updateList
        #app = adsk.core.Application.get()
        #ui  = app.userInterface
        try:
            # Get the command that was executed.
            eventArgs = adsk.core.CommandEventArgs.cast(args)
            # cmd = eventArgs.command
            
            # Process Termination Reason
            if eventArgs.terminationReason == adsk.core.CommandTerminationReason.AbortedTerminationReason: 	
            # 3 	The command is terminated by clicking OK button, and executed failed.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.CancelledTerminationReason:
            #2 	The command is terminated by clicking Cancel button.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.CompletedTerminationReason:
            #1 	The command is terminated by clicking OK button, and executed successfully.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.PreEmptedTerminationReason:
            #4 	The command is terminated by activating another command.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.SessionEndingTerminationReason:
            #5 	The command is terminated by closing the document.
                pass
            elif eventArgs.terminationReason == adsk.core.CommandTerminationReason.UnknownTerminationReason:
            #0 	The command is terminated out of the reasons list below.
                pass
            
            # Handle destruction of all elements
            _handlers.clear()
            _updateDialog = None
            _updateList = None
            
        except:
            _handlers.clear()
            _updateDialog = None
            _updateList = None
            #if ui:
                #ui.messageBox('EquationCurveToolDestroyHandler.notify failed:\n{}'.format(traceback.format_exc()))
            #eventArgs.terminationReason
            #eventArgs.executeFailedMessage = 'EquationCurveToolExecuteHandler.notify failed:\n{}'.format(traceback.format_exc())


# Command Execution Handler
class UpdateEquationCurvesExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            # print("UpdateEquationCurvesExecuteHandler")
            app = adsk.core.Application.get()            
            ui  = app.userInterface
            
            global _updateList
            
            # Get the command that was executed.
            eventArgs = adsk.core.CommandEventArgs.cast(args)
            # cmd = eventArgs.command
            
            # Perform the task now.
            # For each equation curve
            for curve in _updateList:
                if True == curve.build():
                    curve.serialize()
                else:
                    eventArgs.executeFailed = True
                    eventArgs.executeFailedMessage = "Curve build failed."
                    eventArgs.isValidResult = False
                
            # TODO - Force a global recompute, if possible.
            
        except:
            if ui:
                ui.messageBox('UpdateEquationCurvesExecuteHandler.notify failed:\n{}'.format(traceback.format_exc()))
            eventArgs.executeFailed = True
            eventArgs.executeFailedMessage = 'UpdateEquationCurvesExecuteHandler.notify failed:\n{}'.format(traceback.format_exc())


# Return a list of equationCurves
def getEquationCurves():
    try:
        app = adsk.core.Application.get()
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)
            
        curveList = []
            
        # Iterate over the components
        #for c in [design.activeComponent]:#design.allComponents:
        #    comp = adsk.fusion.Component.cast(c)
        attribs = design.findAttributes(attributeGroupName, attributeCurveName)
           
        # Each item on the list is an equation curve
        if attribs is not None:
            for a in attribs:
                if a.parent is not None:
                    curveList.append(EquationCurve.deserialize(a.parent))
            
        return curveList
        
    except:
        app = adsk.core.Application.get()
        ui = app.userInterface
        ui.messageBox("getEquationCurves() Failed!")
        return []

class UpdateEquationCurvesCommandCreatedHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            app = adsk.core.Application.get()
            ui = app.userInterface
            
            global _updateList
            
            #print("UpdateEquationCurvesCommandCreatedHandler")
            
            # Get the command.
            eventArgs = adsk.core.CommandCreatedEventArgs.cast(args)
            cmd = adsk.core.Command.cast(eventArgs.command)
            cmd.isRepeatable = False
            cmd.isAutoExecute = True
            
            # Iterate through every sketch in the document, looking for equation curves.
            app = adsk.core.Application.get()
            product = app.activeProduct
            design = adsk.fusion.Design.cast(product)
        
            _updateList = getEquationCurves()
            
            # Create Event handlers
            onExecute = UpdateEquationCurvesExecuteHandler()
            cmd.execute.add(onExecute)
            
            _handlers.append(onExecute)
            
            # Create the user interface
            inputs = cmd.commandInputs
            inputs.addTextBoxCommandInput('statusMsg', 'Update Status', "Ready to update {} equation curves".format(len(_updateList)), 2, True)
            
        except:
            if ui:
                ui.messageBox('EquationCurveToolCommandCreatedHandler.notify failed:\n{}'.format(traceback.format_exc()))

# Perform AddIn setup
def run(context):
    ui = None
    global _command
    global _updateCmd
    sys.setrecursionlimit(1500)
    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface

        # Create main UI command button
        cmdDefs = ui.commandDefinitions
        cmdDefinition = cmdDefs.addButtonDefinition(c_commandName, '2D/3D Equation Curve', 'Create fitted spline curve from parametric equations', 'Resources/EQCurve')  # TODO - Add button artwork

        # Register command created handler
        onCommandCreated = EquationCurveToolCommandCreatedHandler()
        cmdDefinition.commandCreated.add(onCommandCreated)
        _cHandler.append(onCommandCreated)                     # Store a reference
        
        # Create update all equation curve command
        updateCommandDefinition = cmdDefs.addButtonDefinition(c_updateCommandName, 'Recalculate Equation Curves', 'Performs an update to all existing equation curves', 'Resources/EQCurve') # TODO - Change to a custom icon

        # Register event handler        
        onSubCommandCreated = UpdateEquationCurvesCommandCreatedHandler()
        updateCommandDefinition.commandCreated.add(onSubCommandCreated)
        _cHandler.append(onSubCommandCreated)
        
        # Uncomment this to make a debug listing of allToolbarPanels
        #for panel in ui.allToolbarPanels:
        #    print(panel.id)
        
        sketchPanel = ui.allToolbarPanels.itemById(c_DesignSketchCreate)
        UtilityPanel = ui.allToolbarPanels.itemById(c_DesignToolsUtility)
        
        _command = sketchPanel.controls.addCommand(cmdDefinition)
        _updateCmd = UtilityPanel.controls.addCommand(updateCommandDefinition)
        _updateCmd.isPromoted = True
        
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))



# Unload all UI elements
def stop(context):
    ui = None
    global _command
    global _updateCmd

    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface
        
        # Remove Toolbar controls
        for panelId, name in [(c_DesignSketchCreate,c_commandName), (c_DesignToolsUtility,c_updateCommandName)]:
            panel = ui.allToolbarPanels.itemById(panelId)
            control = panel.controls.itemById(name)
            if control:
                control.deleteMe()
            else:
                print("removal of {} from {} failed.".format(name,panelId))
        
        # Remove UI commands
        for commandName in [c_commandName, c_updateCommandName]:
            cmdDef = ui.commandDefinitions.itemById(commandName)
            if cmdDef:
                cmdDef.deleteMe()
            else:
                print("{} removal failed.".format(commandName))
            
        _cHandler.clear()
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

