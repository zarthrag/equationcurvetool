# Fusion 360 Equation Curve

Autodesk Fusion360 AddIn that adds support for 2D &amp; 3D equation-driven curves.  The curve is implemented using a fittedSpline with fit points placed on points whose locations are determined by equations provided by the user.  Curves can be driven using equations composed of user/model parameters, and can be updated using a global recalculate command.

## Before use:

1. Create any necessary user parameters to facilitate you equation.
2. Create a placeholder user parameter to facilitate calculation (default parameter is assumed to be 't')
3. Create/edit a sketch to place your equation curve on.
4. In the DESIGN workspace, select Sketch->Create->2D/3D Equation Curve.

Once the dialog appears, create your curve by doing the following:
1. Create/Edit Equation Curve - "Create New Curve" or select an existing curve (on this sketch) from the dropdown.
2. Equation Curve Name - Select a unique name for your equation curve.  If placing multiple equation curves on a single sketch, each name must be unique. By default, each curve will be named "*EquationCurve0*", "*EquationCurve1*", etc.

### To create a 2D Equation Curve:
1. For "Curve Dimension", click the "2D" option.
2. For the "Curve Type" dropdown, select one of the following:
   * Parametric. Uses two equations to evaluate X and Y, or r and θ.
   * Explicit. Uses one equation to evaluate Y or r, and a rage for X or a.
3. Choose a coordinate system:
   * Cartesian. Specifies X and Y coordinates on a plane.
   * Polar. Specifies a radial distance (r) and an angle from a fixed direction (a).
4. Enter values.

    * x(t) or r(t). (Parametric) Specify x or r as a function of t. For example x(t): t^2.
    * y(t) or θ(t). (Parametric) Specify y or θ as a function of t. For example, y(t): t^2.
    * y(x) or r(a). (Explicit) Specify an equation for y or r as a function or x or a. For example, y(x): x^2.
    * t/x/θ Range:
        + n Parameter :Depending on the coordinate system, you must specify the user parameter to be swept. (Default for t/x/θ is t/x/theta, respectively)
        + n Start: Specifies the start of the sweep range.
        + n Finish: Specifies the end of the sweep range.
        + n Step: Specifes the change in t/x/theta each step. Choose to optimize number of spline control points.

### To create a 3D Equation Curve:
1. For "Curve Dimension", click the "3D" option.
2. Choose a coordinate system:
   * Cartesian. Specifies X and Y coordinates on a plane.
   * Cylindrical. Specifies radial distance (r), polar angle (θ), and a Z coordinate.
   * Spherical. Specifies radial distance (r), azimuthal angle (ϕ), and polar angle (θ). 
4. Enter values.

    * x(t) or r(t). Specify x or r as a function of t. For example x(t): t^2.
    * y(t) or θ(t) or ϕ(t). Specify y, θ, or ϕ as a function of t. For example, y(t): t^2. 
    * z(t) or θ(a). Specify z or θ as a function of t. For example, z(t): t^2.
    * t Range:
        + t Parameter :Depending on the coordinate system, you must specify the user parameter to be swept. (Default is t)
        + t Start: Specifies the start of the sweep range.
        + t Finish: Specifies the end of the sweep range.
        + t Step: Specifes the change in t each step. Choose to optimize number of spline control points.
5. A preview will display, press 'OK' to create the equation curve-driven spline.

### Edit Equation Curves
To edit an equation curve, do the following:
   * Open the sketch containing the Equation curve for editing.
	 * From the Design->Sketch->Create->2D/3D Equation Curve to open the equation curve dialog.
	 * From the Create/Edit Equation Curve dropdown, select the curve to edit.
	 * Make any changes to the equation curve, as normal.
	 * Press 'OK' To preserve changes.

### Update All Equation Curves
To update/recompute all equation curves after changing parameter values:
   1. Make changes to the variables required.
   2. In the DESIGN workspace, from the toolbar, select Tools->Utility->Recalculate Equation Curves
   3. A dialog showing the number of equation curves found in the document will display.
   4. Press "OK" to begin updating. (Or cancel to abort, the action can also be undone.)

## Known Issues
1. after updating a parameter name, the equation curve will never update the renamed variable in the same way native fusion 360 parameters do.
     > **Reason:** The fusion API doesn't provide a way to identify parameters via an id other than the actual parameter name itself, making a proper response to an updated parameter name virtually impossible.
2. After changing a parameter value, the equation curve does not immediately reflect the new values.
     > **Reason:** Due to API limitations imposed by Fusion360, it's not yet possible to automatically update equation curves if dependent variables change.
     * **Workaround:** Manually update the curve by using the Edit functionality. Alternatively, use the Design>Tools>Utility>Recalculate Equation Curves command to have the plugin recurisvely update all equation curves in the design.  There are plans to address this in a future version of the application.
3. After editing an equation curve, all constraints are removed.
     > **Reason:** Due to API limitations imposed by Fusion360, it's not possible to pragmatically add fit points to a fittedSpline. If a spline requires more fit points to implement an edit, it will instead destory/recreate the curve, leaving all constraints broken.
     
## Changelist
### V1
Initial Release Version

## Author
Anthony Clay, ZarthCode LLC

anthony.clay@zarthcode.com

